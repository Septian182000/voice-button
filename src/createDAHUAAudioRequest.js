import axios from "axios";
import md5 from "js-md5";

export const createDAHUAAudioRequest = async (data) => {
  const form = new FormData();
  form.append("postAudio", data);
  axios
    .post(
      "https://45.64.99.242:8026/cgi-bin/audio.cgi?action=postAudio&httptype=singlepart&channel=1",
      "1"
    )
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      const username = "admin";
      const password = "admin.admin";
      const nc = "00000002";
      const cnonce = "0a00da0a";
      const method = "POST";
      const URI =
        "/cgi-bin/audio.cgi?action=postAudio&httptype=singlepart&channel=1";

      var headersData = error.response.headers["www-authenticate"];
      const realm = headersData
        .split(", ")[0]
        .replaceAll(`"`, "")
        .replace("Digest realm=", "");
      const nonce = headersData
        .split(", ")[2]
        .replaceAll(`"`, "")
        .replace("nonce=", "");
      const qop = headersData
        .split(", ")[1]
        .replaceAll(`"`, "")
        .replace("qop=", "");

      const HA1 = md5.hex(username + ":" + realm + ":" + password);
      const HA2 = md5.hex(method + ":" + URI);
      const res = md5.hex(
        HA1 + ":" + nonce + ":" + nc + ":" + cnonce + ":" + qop + ":" + HA2
      );

      var callbackHeaders = error.response.headers["www-authenticate"];
      callbackHeaders += `, username="${username}", nc=${nc}, cnonce="${cnonce}", response="${res}", uri="${URI}", algorithm="MD5"`;

      const form = new FormData();
      form.append("postAudio", data);

      axios
        .post(
          "http://45.64.99.242:8026/cgi-bin/audio.cgi?action=postAudio&httptype=singlepart&channel=1",
          form,
          {
            headers: {
              Authorization: callbackHeaders,
              // "Content-Type": "multipart/x-mixed-replace; boundary=<boundary>",
            },
          }
        )
        .then((response) => {
          console.log(response.status);
        })
        .catch((e) => {
          console.log(e);
        })
        .finally(() => {
          return true;
        });
    });
};
